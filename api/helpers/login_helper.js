const {request_POST, request} = require('../../base_requests');


const LOGIN_JSON = (testData) => {
	const {personalData: {email}, passwordData: {password}} = testData;

	return {
		login: email,
		password
	};
};

const login_POST = async (testData) => request_POST({
	url: '/web/public/client/login',
	body: LOGIN_JSON(testData)
});


const login_POST = async (testData) => request
	.post('/web/public/client/login')
	.send(LOGIN_JSON(testData))
	.catch(err => err.response);


module.exports = {
	login_POST
};