const {API_URL} = require('../utils/env_config');
const chai = require('chai');
const chaiHttp = require('chai-http');
const addContext = require('mochawesome/addContext');


chai.use(chaiHttp);
const request = chai.request(API_URL);

const AddResponseBody = (ctx, body) => {
    if (ctx.currentTest.state !== 'passed') {
        addContext(ctx, {title: ctx.currentTest.fullTitle(), value: body});
    }
};

/**
 * Base POST request.
 * @memberOf BaseMethods
 * @param {Object} [params] - Options
 * @param {string} params.url - Url for requesting.
 * @param {string} [params.bearerToken] - Auth token.
 * @param {object} [params.body] - Data for sending.
 * @returns {Promise<ChaiHttp.Response>} - Response object
 * @throws {LogResponseError}
 */
const request_POST = (params) => request
	.post(params.url)
	// .set('Content-type', 'application/json') // TODO need discuss with developers
	.auth(params?.bearerToken, {type: 'bearer'})
	.send(params?.body)
	.catch(err => err.response)

/**
 * Base PUT request.
 * @memberOf BaseMethods
 * @param {Object} [params] - Options
 * @param {string} params.url - Url for requesting.
 * @param {string} [params.bearerToken] - Auth token.
 * @param {object} [params.body] - Data for sending.
 * @returns {Promise<ChaiHttp.Response>} - Response object
 * @throws {LogResponseError}
 */
const request_PUT = (params) => request
	.put(params.url)
	.set('Content-type', 'application/json')
	.auth(params?.bearerToken, {type: 'bearer'})
	.send(params?.body)
	.catch(err => err.response)

/**
 * Base GET request.
 * @memberOf BaseMethods
 * @param {Object} [params] - Options
 * @param {string} params.url - Url for requesting.
 * @param {string} [params.bearerToken] - Auth token.
 * @returns {Promise<ChaiHttp.Response>} - Response object
 * @throws {LogResponseError}
 */
const request_GET = (params) => request
	.get(params.url)
	.set('Content-type', 'application/json')
	.auth(params?.bearerToken, {type: 'bearer'})
	.catch(err => err.response)


module.exports = {
    AddResponseBody,
    request,
};
