const {AddResponseBody} = require('../helpers/base');
const loginHelper = require('../helpers/login_helper');


let res;

describe('Example login', () => {

	afterEach(function () {
		AddResponseBody(this, res.body);
	});

	context('login with credentials', () => {

		it('should login with valid credentials /POST', async () => {
			res = await loginHelper.login_POST('testLogin', 'testPassword');

			res.should.have.status(200);
			res.should.be.json;
			res.body.should.be.a('object');
			res.body.should.have.property('user_id', 1);
		});

		it('should not login with invalid password /POST', async () => {
			res = await loginHelper.login_POST('testLogin', 'invalidPassword');

			res.should.have.status(403);
			res.should.be.json;
			res.body.should.be.a('object');
			res.body.should.have.property('error', 'Wrong password');
		});

		it('should not login with invalid user /POST', async () => {
			res = await loginHelper.login_POST('invalidLogin', 'testPassword');

			res.should.have.status(403);
			res.should.be.json;
			res.body.should.be.a('object');
			res.body.should.have.property('error', 'Wrong password');
		});

		it('should not login without creadentials /POST', async () => {
			res = await loginHelper.login_POST('invalidLogin', 'testPassword');

			res.should.have.status(403);
			res.should.be.json;
			res.body.should.be.a('object');
			res.body.should.have.property('error', 'Wrong password');
		});

	});

	context('login with token', () => {

		it('should login with valid token /POST', async () => {
			res = await loginHelper.login_with_token_POST('exampleToken');

			res.should.have.status(200);
			res.should.be.json;
			res.body.should.be.a('object');
			res.body.should.have.property('user_id', 1);
		});

		it('should login without token /POST', async () => {
			res = await loginHelper.login_with_token_POST();

			res.should.have.status(403);
			res.should.be.json;
			res.body.should.be.a('object');
			res.body.should.have.property('error', 'some error');
		});


	});
});
